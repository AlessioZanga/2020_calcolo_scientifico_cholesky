% --- DEFINE FUNCTION TO SOLVE SPARSE MATRIX USING CHOLESKY DECOMPOSITION ---%

function [name, size, time, memory, relativeError] = profileSparseCholeskySolver(A)

    % Extract A, matrix size and name from file
    A = A{1};
    name = extractAfter(A.name,"/");
    A = A.A;
    size = length(A);
        
    % Plot non zero elements before permutation
    figure,
    perc = 100 / numel(A);
    subplot(1, 3, 1),
    spy(A),
    title(strcat(name, ' before permutation'));
    nz = nnz(A);
    xlabel(sprintf('Nonzeros = %d (%.3f%%)', nz, nz*perc));
        
    % Initialize Xe base vector
    Xe = ones(size, 1);
    
    % Enable profiling session
    profile -memory on

    % Performing preliminary permutation of elements
    p = amd(A);

    % Computing b = A * Xe
    b = A(p, p) * Xe;

    % Compute cholesky factorization
    R = chol(A(p, p));

    % Solve the system using backsolver and forwardsolver
    % R'* y = b
    % R * x = y
    Xapp = R\(R'\b);    

    % Disable profiling session
    profile off
    profileInfo = profile('info');
    
    % Save profiling data
    % File name and folder
    n = strcat('profiles/profile_', name);
    save(n, 'profileInfo');
    
    % Plot non zero elements after permutation
    subplot(1, 3, 2),
    spy(A(p, p)),
    title(strcat(name, ' after permutation'))
    nz = nnz(A);
    xlabel(sprintf('Nonzeros = %d (%.3f%%)', nz, nz*perc));

    % Plot non zero elements after Cholesky factorization
    subplot(1, 3, 3),
    spy(R)
    title(strcat(name, ' after Cholesky factorization'))
    nc(5) = nnz(R);
    xlabel(sprintf('Nonzeros = %d (%.2f%%)', nc(5), nc(5)*perc));

    % Execution time to compute Cholesky factorization
    % and solve the linear system
    time = profileInfo.FunctionTable.TotalTime;

    % Total memory allocated
    tb = struct2table(profileInfo.FunctionTable);
    memory = tb(strcmp('profileSparseCholeskySolver', tb.FunctionName), :).TotalMemAllocated;

    % Compute relative error
    relativeError = norm(Xapp - Xe) / norm(Xe);

end