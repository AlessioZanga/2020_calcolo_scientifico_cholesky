% Clear all variables and close all windows
close all
clear all
clc


% --- LOAD MATLAB PROFILE DATA --- %


% Load MATLAB linux profile
lm = load('linux_profiles_matlab.mat');
lm = lm.t;

% Load MATLAB windows profile
wm = load('windows_profiles_matlab.mat');
wm = wm.t;

% Extracting data from MATLAB linux profiling
labels = lm.name;
msize = horzcat(lm.size{:});
lmTime = horzcat(lm.time{:});
lmMemory = horzcat(lm.memory{:});
lmRelativeError = horzcat(lm.relativeError{:});

% Extracting data from MATLAB windows profiling
wmTime = horzcat(wm.time{:});
wmMemory = horzcat(wm.memory{:});
wmRelativeError = horzcat(wm.relativeError{:});


% --- LOAD R PROFILE DATA --- %


% Current folder
codeFolder = cd;

% Moving to Data folder
cd ../R
            
% Load R linux profile
lr = load('linux_profiles.rds.mat');

% Converting loaded data into table
lr.size = num2cell(lr.size)';
lr.time = num2cell(lr.time)';
lr.memory = num2cell(lr.memory)';
lr.error = num2cell(lr.error)';
lr = struct2table(lr);

% sort matrices by size
lr = sortrows(lr, 'size');

% Load R windows profile
wr = load('windows_profiles.rds.mat');

% Move back to previous folder
cd(codeFolder);

% Converting loaded data into table
wr.size = num2cell(wr.size)';
wr.time = num2cell(wr.time)';
wr.memory = num2cell(wr.memory)';
wr.error = num2cell(wr.error)';
wr = struct2table(wr);

% sort matrices by size
wr = sortrows(wr, 'size');

% Extracting data from R linux profiling
rsize = double(horzcat(lr.size{:}));
lrTime = horzcat(lr.time{:});
lrMemory = horzcat(lr.memory{:});
lrRelativeError = horzcat(lr.error{:});

% Extracting data from R windows profiling
wrTime = horzcat(wr.time{:});
wrMemory = horzcat(wr.memory{:});
wrRelativeError = horzcat(wr.error{:});


% --- PLOT DATA ---%


% Plot profiling data to compare each resource on each Environment/OS
plotResouce('Time', lmTime, wmTime, lrTime, wrTime, msize, rsize, labels);

plotResouce('Memory', lmMemory, wmMemory, lrMemory, wrMemory, msize, rsize, labels);

plotResouce('Relative Error', lmRelativeError, wmRelativeError, ...
            lrRelativeError, wrRelativeError, msize, rsize, labels);


% Plot profiling data to compare each Environment on each OS
plotEnvironment('MATLAB', lmTime, lmMemory, lmRelativeError, ...
        wmTime, wmMemory, wmRelativeError, msize, labels);
        
plotEnvironment('R', lrTime, lrMemory, lrRelativeError, ...
        wrTime, wrMemory, wrRelativeError, rsize, labels);
        


% Defining function to plot a specific profiled resource
% to compare MATLAB  and R
function  plotResouce(resourceName, lmResource, wmResource, ...
                      lrResource, wrResource, msize, rsize, labels)

    figure,
    % MATLAB
    semilogy(msize, lmResource, 'color', 'blue', 'Marker', 'o')
    hold on
    semilogy(msize, wmResource, 'color', 'red', 'Marker', 'o');
    
    % R
    semilogy(rsize, lrResource, 'color', '#4DBEEE', 'Marker', 'o');
    semilogy(rsize, wrResource, 'color', '#D95319', 'Marker', 'o');

    % Set axis labels
    xlabel('Matrix Size');
    ylabel(resourceName);

    % Set legend
    legend('M-Linux', 'M-Windows', 'R-Linux', 'R-Windows', 'Location', 'best');

    % Set X axis values (Matrix size)
    set(gca, 'xtick', msize);

    % Set position of second X axis
    ax1 = gca;
    ax1.XTickLabelRotation = 45;
    pos1 = ax1.Position;
    ax2 = axes('Position', pos1, 'XAxisLocation', 'top', ...
        'YAxisLocation','Right', 'Color', 'none');

    % Set the same Limits and Ticks on ax2 as on ax1;
    set(ax2, 'XLim', get(ax1, 'XLim'),'YLim', get(ax1, 'YLim'));
    set(ax2, 'XTick', get(ax1, 'XTick'), 'YTick', get(ax1, 'YTick'));

    % Set the x-tick and y-tick  labels for the second axes
    set(ax2, 'XTickLabel', labels, 'YTickLabel',[]);
    ax2.XTickLabelRotation = 90;

    hold off
    
end


% Defining function to plot all profiled resource  in MATLAB and R
% on a specific OS, to compare the behaviour of each environment on
% Linux and Windows
function plotEnvironment(environment, lTime, lMemory, lRelativeError, ...
        wTime, wMemory, wRelativeError, size, labels)
   
    figure,
    % Linux
    semilogy(size, lTime, 'color', 'red', 'Marker', 'o');
    hold on
    semilogy(size, lMemory, 'color', 'green', 'Marker', 'o');
    semilogy(size, lRelativeError, 'color', 'blue', 'Marker', 'o');
    
    % Windows
    semilogy(size, wTime, 'color', '#D95319', 'Marker', 'o');
    semilogy(size, wMemory, 'color', '#77AC30', 'Marker', 'o');
    semilogy(size, wRelativeError, 'color', '#4DBEEE', 'Marker', 'o');

    % Set axis labels
    xlabel('Matrix Size');
    ylabel('Profiled Resource');
    
    % Set tile
    title(environment);

    % Set X axis values (Matrix size)
    set(gca, 'xtick', size);

    % Set legend
    legend('timeL', 'memoryL', 'relativeErrorL', ... 
           'timeW', 'memoryW', 'relativeErrorW', 'Location', 'best');

    % Set position of second X axis (for matricex names)
    ax1 = gca;
    ax1.XTickLabelRotation = 45;
    pos1 = ax1.Position;
    ax2 = axes('Position', pos1, 'XAxisLocation', 'top', ...
        'YAxisLocation','Right', 'Color', 'none');

    % Set the same Limits and Ticks on ax2 as on ax1;
    set(ax2, 'XLim', get(ax1, 'XLim'),'YLim', get(ax1, 'YLim'));
    set(ax2, 'XTick', get(ax1, 'XTick'), 'YTick', get(ax1, 'YTick'));

    % Set the x-tick and y-tick  labels for the second axes
    set(ax2, 'XTickLabel', labels, 'YTickLabel', []);
    ax2.XTickLabelRotation = 90;

    hold off
end