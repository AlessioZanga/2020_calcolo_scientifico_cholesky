% Clear all variables and close all windows
close all
clear all
clc


% --- LOAD MATRICES AND PROFILING THE CODE  --- %


% Create folder to save profiles
if ~exist('profiles', 'dir')
    mkdir('profiles');
end


% Current folder
codeFolder = cd;

% Moving to Data folder
cd ../data

% Load matrix object from file
path = dir(fullfile(cd, '*.mat'));
matrices = arrayfun(@(mi) load(fullfile(cd, mi.name)), path, ...
                'UniformOutput', false);
            
% Move back to previous folder
cd(codeFolder);

% Extract Problem from objects
matrices = cellfun(@(mi) mi.Problem, matrices, 'UniformOutput', false);

% Solve each matrix one by one using Cholesky decomposition
% and profiling code
[name, size, time, memory, relativeError] = ...
    arrayfun(@(mi) profileSparseCholeskySolver(mi), ...
    matrices, 'UniformOutput', false);


% --- ORDERING DATA BY MATRIX SIZE AND SAVE PROFILING INFORMATIONS --- %


% sort matrices by size
t = table(name, size, time, memory, relativeError);
t = sortrows(t, 'size');

% Save profile data to plot
save('profiles/matlabProfile', 't');